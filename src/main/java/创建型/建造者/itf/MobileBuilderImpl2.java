package 创建型.建造者.itf;

import 创建型.建造者.base.AbstractBasePackage;
import 创建型.建造者.model.MobilePackage;

/**
 * Created by 73546 on 2017/4/24.
 */
public class MobileBuilderImpl2 extends AbstractBasePackage implements IMobileBuilder{
    

    // 建造手机套餐的话费
    public void buildMoney() {
        this.mobilePackage.setMoney(30.0f);
    }

    // 建造手机套餐的短信
    public void buildShortInfo() {
        this.mobilePackage.setShortInfo(600);
    }

    // 建造手机套餐的彩信
    public void buildMusic() {
        this.mobilePackage.setMusic("老人与海");
    }

    public MobilePackage getMobilePackage() {

        return this.mobilePackage;
    }
}

package 创建型.建造者.itf;

import 创建型.建造者.model.MobilePackage;

/**
 * 手机套餐 Builder
 *
 * Created by 73546 on 2017/4/24.
 */
public interface IMobileBuilder {

    // 建造手机套餐的话费
    public void buildMoney();

    // 建造手机套餐的短信
    public void buildShortInfo();

    // 建造手机套餐的彩铃
    public void buildMusic();

    // 返回建造的手机套餐对象
    public MobilePackage getMobilePackage();
}

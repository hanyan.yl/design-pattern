package 创建型.建造者.base;

import 创建型.建造者.model.MobilePackage;

/**
 * Created by 73546 on 2017/4/24.
 */
public abstract class AbstractBasePackage {

    // 手机套餐实例变量
    protected MobilePackage mobilePackage;

    public AbstractBasePackage() {
        this.mobilePackage = new MobilePackage();
    }
}

package 创建型.建造者;

import 创建型.建造者.director.MobileDirector;
import 创建型.建造者.itf.MobileBuilderImpl1;
import 创建型.建造者.itf.MobileBuilderImpl2;
import 创建型.建造者.model.MobilePackage;

/**
 * Created by 73546 on 2017/4/24.
 */
public class MainApp {

    public static void main(String[] args) {
        // 创建指导者
        MobileDirector mobileDirector = new MobileDirector();
        // 套餐1
        MobileBuilderImpl1 mobileBuilderImpl1 = new MobileBuilderImpl1();
        // 套餐2
        MobileBuilderImpl2 mobileBuilderImpl2 = new MobileBuilderImpl2();

        printMessage(mobileDirector.createMobilePackage(mobileBuilderImpl1));
        printMessage(mobileDirector.createMobilePackage(mobileBuilderImpl2));

    }

    public static void printMessage(MobilePackage mobilePackage){
        System.out.println("--话费："+ mobilePackage.getMoney() + "\t短信："
        + mobilePackage.getShortInfo() + "条\t彩铃："+ mobilePackage.getMusic());
    }
}

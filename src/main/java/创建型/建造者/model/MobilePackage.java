package 创建型.建造者.model;

/**
 *
 * 手机套餐
 * Created by 73546 on 2017/4/24.
 */
public class MobilePackage {

    // 话费
    private float money;

    // 短信
    private int shortInfo;

    // 彩铃
    private String music;

    public float getMoney() {
        return money;
    }

    public void setMoney(float money) {
        this.money = money;
    }

    public int getShortInfo() {
        return shortInfo;
    }

    public void setShortInfo(int shortInfo) {
        this.shortInfo = shortInfo;
    }

    public String getMusic() {
        return music;
    }

    public void setMusic(String music) {
        this.music = music;
    }
}

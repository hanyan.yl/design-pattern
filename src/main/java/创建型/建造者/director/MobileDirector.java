package 创建型.建造者.director;

import 创建型.建造者.itf.IMobileBuilder;
import 创建型.建造者.model.MobilePackage;

/**
 *
 * 手机套餐指导者
 * Created by 73546 on 2017/4/24.
 */
public class MobileDirector {

    public MobilePackage createMobilePackage(IMobileBuilder mobileBuilder){

        if(mobileBuilder != null){
            // 构建话费
            mobileBuilder.buildMoney();
            // 构建短信
            mobileBuilder.buildShortInfo();
            // 构建彩铃
            mobileBuilder.buildMusic();
            // 返回手机套餐
            return mobileBuilder.getMobilePackage();
        }

        return null;
    }
}

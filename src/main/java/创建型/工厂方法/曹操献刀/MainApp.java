package 创建型.工厂方法.曹操献刀;

import 创建型.工厂方法.曹操献刀.itf.ISwordFactory;
import 创建型.工厂方法.曹操献刀.model.AbstractSword;

/**
 * Created by 73546 on 2017/4/24.
 */
public class MainApp {

    public static void main(String[] args) {

        /********* 使用七星宝刀 ***********/
        // 创建曹操实例对象
        ISwordFactory swordFactory = new CaoCao();
        // 获得七星宝刀
        AbstractSword sword = swordFactory.createSword();
        // 刺杀董卓
        System.out.println("曹操使用"+ sword.getName() + "刺杀董卓！");

        /********** 使用菜刀 **********/
        // 创建曹操实例对象
        ISwordFactory swordFactory2 = new CaoCao2();
        // 获得七星宝刀
        AbstractSword sword2 = swordFactory2.createSword();
        // 刺杀董卓
        System.out.println("曹操使用"+ sword2.getName() + "刺杀董卓！");
    }

}

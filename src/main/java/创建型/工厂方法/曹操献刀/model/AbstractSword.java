package 创建型.工厂方法.曹操献刀.model;

/**
 * Created by 73546 on 2017/4/24.
 */
public abstract class AbstractSword {

    // 宝刀名称
    private String name;

    // 构造方法
    public AbstractSword() {
    }

    // 获取名称
    public String getName() {
        return name;
    }

    // 设置名称
    public void setName(String name) {
        this.name = name;
    }
}

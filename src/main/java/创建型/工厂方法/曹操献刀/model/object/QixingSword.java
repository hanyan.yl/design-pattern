package 创建型.工厂方法.曹操献刀.model.object;

import 创建型.工厂方法.曹操献刀.model.AbstractSword;

/**
 * Created by 73546 on 2017/4/24.
 */
public class QixingSword extends AbstractSword{

    /**
     * 设置七星宝刀的名称
     */
    public QixingSword(){
        this.setName("七星宝刀");
    }
}

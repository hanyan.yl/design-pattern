package 创建型.工厂方法.曹操献刀;

import 创建型.工厂方法.曹操献刀.itf.ISwordFactory;
import 创建型.工厂方法.曹操献刀.model.AbstractSword;
import 创建型.工厂方法.曹操献刀.model.object.CaiDao;

/**
 * 曹操具体工程
 *
 * Created by 73546 on 2017/4/24.
 */
public class CaoCao2 implements ISwordFactory {

    public AbstractSword createSword() {

        return new CaiDao();
    }
}

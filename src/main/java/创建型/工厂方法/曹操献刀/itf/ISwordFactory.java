package 创建型.工厂方法.曹操献刀.itf;

import 创建型.工厂方法.曹操献刀.model.AbstractSword;

/**
 * Created by 73546 on 2017/4/24.
 */
public interface ISwordFactory {

    /**
     * 生产各类宝刀
     * @return
     */
    public AbstractSword createSword();

}

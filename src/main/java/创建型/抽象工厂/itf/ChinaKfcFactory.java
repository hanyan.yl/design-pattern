package 创建型.抽象工厂.itf;

import 创建型.抽象工厂.model.Beverage;
import 创建型.抽象工厂.model.ChickenWings;
import 创建型.抽象工厂.model.FrenchFries;
import 创建型.抽象工厂.model.Hamburg;
import 创建型.抽象工厂.model.kfc.ChinaBeverage;
import 创建型.抽象工厂.model.kfc.ChinaChickenWings;
import 创建型.抽象工厂.model.kfc.ChinaFrenchFries;
import 创建型.抽象工厂.model.kfc.ChinaHanburm;

/**
 * Created by 73546 on 2017/4/24.
 */
public class ChinaKfcFactory implements IKfcFactory{

    public Hamburg createHamburg(int num) {
        return new ChinaHanburm(num);
    }

    public FrenchFries createFrenchFries(int num) {
        return new ChinaFrenchFries(num);
    }

    public ChickenWings createChickenWings(int num) {
        return new ChinaChickenWings(num);
    }

    // 生产可乐
    public Beverage createBeverage(int num){
        return new ChinaBeverage(num);
    }
}

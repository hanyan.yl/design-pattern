package 创建型.抽象工厂.itf;

import 创建型.抽象工厂.model.Beverage;
import 创建型.抽象工厂.model.ChickenWings;
import 创建型.抽象工厂.model.FrenchFries;
import 创建型.抽象工厂.model.Hamburg;

/**
 * 肯德基抽象工厂
 *
 * Created by 73546 on 2017/4/24.
 */
public interface IKfcFactory {

    // 生产汉堡
    public Hamburg createHamburg(int num);

    // 生产薯条
    public FrenchFries createFrenchFries(int num);

    // 生产鸡翅
    public ChickenWings createChickenWings(int num);

    // 生产饮料
    public Beverage createBeverage(int num);

}

package 创建型.抽象工厂.model.kfc;

import 创建型.抽象工厂.model.Beverage;

/**
 * 可乐
 * Created by 73546 on 2017/4/24.
 */
public class ChinaBeverage extends Beverage {

    public ChinaBeverage(int num) {

        this.kind = "可乐";
        this.price = 7.0f;
        this.num = num;
    }
}

package 创建型.抽象工厂.model.kfc;

import 创建型.抽象工厂.model.Hamburg;

/**
 * 中国风味的麻辣鸡腿汉堡
 * Created by 73546 on 2017/4/24.
 */
public class ChinaHanburm extends Hamburg {

    public ChinaHanburm(int num) {

        this.kind = "麻辣";
        this.price = 14.0f;
        this.num=num;
    }
}

package 创建型.抽象工厂.model.kfc;

import 创建型.抽象工厂.model.ChickenWings;

/**
 * 鸡翅实现类
 * Created by 73546 on 2017/4/24.
 */
public class ChinaChickenWings extends ChickenWings {

    public ChinaChickenWings(int num) {

        this.kind = "奥尔良";
        this.price = 2.5f;
        this.num = num;
    }
}

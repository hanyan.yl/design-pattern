package 创建型.抽象工厂.model.kfc;

import 创建型.抽象工厂.model.FrenchFries;

/**
 * 中国味薯条
 * Created by 73546 on 2017/4/24.
 */
public class ChinaFrenchFries extends FrenchFries {

    public ChinaFrenchFries(int num) {

        this.kind = "普通";
        this.price = 8.0f;
        this.num = num;
    }
}

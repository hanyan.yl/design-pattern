package 创建型.抽象工厂.model;

/**
 *
 * 饮料基类
 * Created by 73546 on 2017/4/24.
 */
public abstract class Beverage extends AbstactBaseFood implements IFood{

    public void printMessage() {
        System.out.println("--"+this.kind + "饮料，\t 单价：" +
        this.price + "，\t 数量：" + this.num + "，\t 合计：" + this.totalPrice());
    }
}

package 创建型.抽象工厂.model;

/**
 * 食物基类
 *
 * Created by 73546 on 2017/4/24.
 */
public abstract class AbstactBaseFood {

    // 类别
    protected String kind;
    // 数量
    protected int num;
    // 价格
    protected float price;
    // 合计
    public float totalPrice(){
        return this.num * this.price;
    }
}

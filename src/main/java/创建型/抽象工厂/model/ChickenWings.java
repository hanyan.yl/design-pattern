package 创建型.抽象工厂.model;

/**
 *
 * 鸡翅基类
 * Created by 73546 on 2017/4/24.
 */
public abstract class ChickenWings extends AbstactBaseFood implements IFood{

    public void printMessage() {
        System.out.println("--"+this.kind + "风味鸡翅，\t 单价：" +
        this.price + "，\t 数量：" + this.num + "，\t 合计：" + this.totalPrice());
    }
}

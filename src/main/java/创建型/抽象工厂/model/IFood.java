package 创建型.抽象工厂.model;

/**
 * Created by 73546 on 2017/4/24.
 */
public interface IFood {

    /**
     * 打印输出食物信息
     */
    void printMessage();
}

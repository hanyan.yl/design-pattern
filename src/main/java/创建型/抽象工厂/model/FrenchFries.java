package 创建型.抽象工厂.model;

/**
 *
 * 薯条基类
 * Created by 73546 on 2017/4/24.
 */
public abstract class FrenchFries extends AbstactBaseFood implements IFood{

    public void printMessage() {
        System.out.println("--"+this.kind + "风味薯条，\t 单价：" +
        this.price + "，\t 数量：" + this.num + "，\t 合计：" + this.totalPrice());
    }
}

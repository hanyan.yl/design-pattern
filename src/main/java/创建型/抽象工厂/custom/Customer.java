package 创建型.抽象工厂.custom;

import 创建型.抽象工厂.itf.IKfcFactory;
import 创建型.抽象工厂.model.Beverage;
import 创建型.抽象工厂.model.ChickenWings;
import 创建型.抽象工厂.model.FrenchFries;
import 创建型.抽象工厂.model.Hamburg;

/**
 * Created by 73546 on 2017/4/24.
 */
public class Customer {

    // 抽象工厂
    private IKfcFactory kfcFactory;

    // 构造方法将抽象工厂作为参数传入
    public Customer(IKfcFactory kfcFactory){
        this.kfcFactory = kfcFactory;
    }

    // 订购食物
    public float orderHamburg(int num){
        // 获得麻辣鸡腿汉堡
        Hamburg hamburg = kfcFactory.createHamburg(num);
        // 输出订购信息
        hamburg.printMessage();
        // 返回总价
        return hamburg.totalPrice();
    }

    // 订购奥尔良烤鸡翅
    public float orderChickenWings(int num){

        // 获得奥尔良烤鸡翅
        ChickenWings chickenWings = kfcFactory.createChickenWings(num);
        // 输出订购信息
        chickenWings.printMessage();
        // 返回总价
        return chickenWings.totalPrice();
    }

    // 订购薯条
    public float orderFrenchFries(int num){

        // 获得薯条
        FrenchFries frenchFries = kfcFactory.createFrenchFries(num);
        // 输出订购信息
        frenchFries.printMessage();
        // 返回总价
        return frenchFries.totalPrice();
    }

    // 订购可乐
    public float orderBeverage(int num){

        // 获得可乐
        Beverage beverage = kfcFactory.createBeverage(num);
        // 输出订购信息
        beverage.printMessage();
        // 返回总价
        return beverage.totalPrice();
    }
}

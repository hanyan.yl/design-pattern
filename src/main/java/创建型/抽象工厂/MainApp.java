package 创建型.抽象工厂;

import 创建型.抽象工厂.custom.Customer;
import 创建型.抽象工厂.itf.ChinaKfcFactory;
import 创建型.抽象工厂.itf.IKfcFactory;

/**
 * Created by 73546 on 2017/4/24.
 */
public class MainApp {

    public static void main(String[] args) {

        IKfcFactory kfcFactory = new ChinaKfcFactory();

        Customer customer = new Customer(kfcFactory);

        float hamburgMoney = customer.orderHamburg(1);

        float chickenWingsMoney = customer.orderChickenWings(4);

        float frenchFriesMoney = customer.orderFrenchFries(1);

        float beverageMoney = customer.orderBeverage(2);

        System.out.println("总计："+ (hamburgMoney + chickenWingsMoney +
                frenchFriesMoney + beverageMoney));
    }
}

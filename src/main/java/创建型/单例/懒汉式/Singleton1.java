package 创建型.单例.懒汉式;

/**
 * Created by 73546 on 2017/4/25.
 */
public class Singleton1 {

    private static Singleton1 singleton1 = new Singleton1();

    private Singleton1(){
        System.out.println("-- this is Singleton !!!");
    }

    // 获得单例方法
    public static Singleton1 getInstance(){
        return singleton1;
    }
}

package 创建型.单例.多例;

/**
 * Created by 73546 on 2017/4/25.
 */
public class MultipletonClient {

    public static void main(String[] args) {
        Multipleton multipleton = Multipleton.getRandomInstance();
        System.out.println("multipleton:"+multipleton.getNo());
        // 再次获得Multipleton对象实例
        Multipleton multipleton2 = Multipleton.getRandomInstance();
        System.out.println("multipleton2:"+multipleton2.getNo());
        // 比较两个对象是否是同一对象实例
        if(multipleton == multipleton2){
            System.out.println("这是同一对象！");
        }else {
            System.out.println("这是不同对象！");
        }
    }
}

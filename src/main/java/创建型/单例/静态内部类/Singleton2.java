package 创建型.单例.静态内部类;

/**
 * Created by 73546 on 2017/4/25.
 */
public class Singleton2 {

    private static class SingletonHolder{
        public static final Singleton2 INSTANCE = new Singleton2();
    }

    private Singleton2 (){}

    public static final Singleton2 getInstance(){
        return SingletonHolder.INSTANCE;
    }
}

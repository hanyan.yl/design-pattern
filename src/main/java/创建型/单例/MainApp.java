package 创建型.单例;

import 创建型.单例.恶汉式.Singleton;

/**
 * Created by 73546 on 2017/4/25.
 */
public class MainApp {

    public static void main(String[] args) {
        Singleton singleton = Singleton.getInstance();

        Singleton singleton1 = Singleton.getInstance();

        if(singleton == singleton1){
            System.out.println("-- 这是同一个对象");
        } else {
            System.out.println("-- 这是不同的对象");
        }
    }
}

package 创建型.单例.恶汉式;

/**
 * Created by 73546 on 2017/4/25.
 */
public class Singleton {

    private static Singleton singleton = null;

    private Singleton(){
        System.out.println("------ 这是singleton类-----");
    }

    public synchronized static Singleton getInstance(){
        if(singleton == null){
            singleton = new Singleton();
        }
        return singleton;
    }
}

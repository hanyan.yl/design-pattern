package 创建型.原型.factory.impl;

import 创建型.原型.factory.ILifeFactory;
import 创建型.原型.pojo.DayLife;

/**
 * Created by 73546 on 2017/4/25.
 */
public class LifeFactoryImpl implements ILifeFactory {

    private static DayLife dayLife = null;

    public DayLife getNewInstance() {

        if(dayLife == null) {
            System.out.println(" new DayLife !");
            dayLife = new DayLife();
            dayLife.setGetUp("7:00 起床");
            dayLife.setByBus("7:30 坐公交");
            dayLife.setGetFood("8:30 吃早餐");
            dayLife.setNoon("12:00 吃午餐");
            dayLife.setAfternoonWork("13:30 开始下午的工作");
            dayLife.setGoHome("17:30 下班回家");
            dayLife.setNight("18:00 晚上休息");
        } else {
            System.out.println(" clone DayLife !");
            dayLife = dayLife.clone();
        }
        return dayLife;
    }
}

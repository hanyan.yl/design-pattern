package 创建型.原型.factory;

import 创建型.原型.pojo.DayLife;

/**
 * Created by 73546 on 2017/4/25.
 */
public interface ILifeFactory {

    public DayLife getNewInstance();
}

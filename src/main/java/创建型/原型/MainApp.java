package 创建型.原型;

import 创建型.原型.factory.ILifeFactory;
import 创建型.原型.factory.impl.LifeFactoryImpl;
import 创建型.原型.pojo.DayLife;

/**
 * Created by 73546 on 2017/4/25.
 */
public class MainApp {

    public static void main(String[] args) {
        ILifeFactory lifeFactory = new LifeFactoryImpl();

        lifeFactory.getNewInstance().print();

        System.out.println("--------------------------");
        DayLife dayLife = lifeFactory.getNewInstance();
        dayLife.setGetUp("早上赖床，7:15才起床");
        dayLife.print();

        // 再次获得DayLife
        System.out.println("--------------------------");
        DayLife dayLife2 = lifeFactory.getNewInstance();
        dayLife2.setGetUp("早上赖床，7:30才起床");
        dayLife2.print();
    }
}

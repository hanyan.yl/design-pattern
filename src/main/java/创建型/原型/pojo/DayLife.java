package 创建型.原型.pojo;

/**
 * Created by 73546 on 2017/4/25.
 */
public class DayLife implements Cloneable {

    // 起床
    private String getUp;
    // 坐公交
    private String byBus;
    private String getFood;
    private String noon;
    private String afternoonWork;
    private String goHome;
    private String night;

    public String getGetUp() {
        return getUp;
    }

    public void setGetUp(String getUp) {
        this.getUp = getUp;
    }

    public String getByBus() {
        return byBus;
    }

    public void setByBus(String byBus) {
        this.byBus = byBus;
    }

    public String getGetFood() {
        return getFood;
    }

    public void setGetFood(String getFood) {
        this.getFood = getFood;
    }

    public String getNoon() {
        return noon;
    }

    public void setNoon(String noon) {
        this.noon = noon;
    }

    public String getAfternoonWork() {
        return afternoonWork;
    }

    public void setAfternoonWork(String afternoonWork) {
        this.afternoonWork = afternoonWork;
    }

    public String getGoHome() {
        return goHome;
    }

    public void setGoHome(String goHome) {
        this.goHome = goHome;
    }

    public String getNight() {
        return night;
    }

    public void setNight(String night) {
        this.night = night;
    }

    /**
     * 打印日常生活
     */
    public void print(){
        System.out.println(this.getGetUp());
        System.out.println(this.getByBus());
        System.out.println(this.getGetFood());
        System.out.println(this.getNoon());
        System.out.println(this.getAfternoonWork());
        System.out.println(this.getGoHome());
        System.out.println(this.getNight());
    }

    @Override
    public DayLife clone() {
        try{
            return (DayLife) super.clone();
        } catch (Exception e){

        }
        return null;
    }
}

设计模式的分类：创建型、结构型和行为型

    创建型模式用来处理对象的创建过程，主要包含以下5种设计模式:
    1）工厂方法模式(Factory Method Pattern)
    2）抽象工厂模式(Abstract Factory Pattern)
    3）建造者模式(Builder Pattern)
    4）原型模式(Prototype Pattern)
    5）单例模式(Singleton Pattern)

    结构型模式用来处理类或者对象的组合，包含7种设计模式：
    1）适配器模式(Adapter Pattern)
    2）桥接模式(Bridge Pattern)
    3）组合模式(Composite Pattern)
    4）装饰者模式(Decorator Pattern)
    5）外观模式(Facade Pattern)
    6）享元模式(Flyweight Pattern)
    7）代理模式(Proxy Pattern)

    行为型模式用来对类或对象怎样交互和怎样分配职责进行描述，主要包含以下11种设计模式:
    1）责任链模式(Chain of Responsibility Pattern)
    2）命令模式(Command Pattern)
    3）解释器模式(Interpreter Pattern)
    4）迭代器模式(Iterator Pattern)
    5）中介者模式(Mediator Pattern)
    6）备忘录模式(Memento Pattern)
    7）观察者模式(Observer Pattern)
    8）状态模式(State Pattern)
    9）策略模式(Strategy Pattern)
    10）模板方法模式(Template Method Pattern)
    11）访问者模式(Visitor Pattern)

++++++++++++++++++++++++++++++++++++++++++
+  代码背景源于《软件秘籍：设计模式那点事》 +
++++++++++++++++++++++++++++++++++++++++++

package 结构型.组合.composite;

import java.util.ArrayList;

/**
 * Created by 73546 on 2017/4/25.
 */
public class Manager extends Staff {

    private final ArrayList<Staff> arrayList = new ArrayList<Staff>();

    public Manager(String no, String name, String position, float salary) {
        super(no, name, position, salary);
    }

    @Override
    public void add(Staff staff) {
        this.arrayList.add(staff);
    }

    @Override
    public Staff remove(String no) {
        Staff staff = null;
        if(no != null && !"".equals(no.trim())){
            for(int i = 0;i<this.arrayList.size();i++){
                if(this.arrayList.get(i) == null){
                    continue;
                }

                if(no.equals(this.arrayList.get(i).getNo())){
                    staff = this.arrayList.remove(i);
                    break;
                }
            }
        }
        return staff;
    }

    @Override
    public void printEmployeesInfo(int layer) {
        int tmpLayer = ++layer;
        for(int i =0;i<this.arrayList.size();i++){
            if(this.arrayList.get(i) == null) continue;
            printChar(tmpLayer);
            this.arrayList.get(i).printUserBaseInfo();
            // 打印手下员工信息
            this.arrayList.get(i).printEmployeesInfo(tmpLayer);
        }
    }
}

package 结构型.组合.composite;

/**
 * Created by 73546 on 2017/4/25.
 */
public class Employees extends Staff {
    public Employees(String no, String name, String position, float salary) {
        super(no, name, position, salary);
    }

    @Override
    public void add(Staff staff) {
        return;
    }

    @Override
    public Staff remove(String no) {
        return null;
    }

    @Override
    public void printEmployeesInfo(int layer) {
        return;
    }
}

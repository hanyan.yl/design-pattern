package 结构型.组合.common;

import java.util.ArrayList;

/**
 * Created by 73546 on 2017/4/25.
 */
public class Manager {

    private String no;
    private String name;
    private String position;
    private float salary;
    private ArrayList arrayList = new ArrayList();
    private int length;

    public Manager(String no, String name, String position, float salary) {
        this.no = no;
        this.name = name;
        this.position = position;
        this.salary = salary;
        this.length +=(no == null || "".equals(no.trim()))?0:no.getBytes().length;
        this.length +=(name == null || "".equals(name.trim()))?0:name.getBytes().length;
        this.length +=(position == null || "".equals(position.trim()))?0:position.getBytes().length;
        this.length +=String.valueOf(salary).getBytes().length;
    }

    /**
     * 增加管理人员
     */
    public void add (Manager manager){
        this.arrayList.add(manager);
    }

    /**
     * 增加一个普通员工
     */
    public void add (Employees employees){
        this.arrayList.add(employees);
    }

    // 打印基本信息
    public void printUserBaseInfo(){
        System.out.println("|"+this.no+" "+this.name+" "+ this.position+" "+ this.salary);
    }

    /**
     * 打印员工信息
     */
    public void printEmployeesInfo(int layer){
        int tmpLayer = ++layer;
        for(int i = 0;i<this.arrayList.size();i++){
            Object object = this.arrayList.get(i);
            if(object == null) continue;
            if(object instanceof Employees){
                printChar(tmpLayer);
                ((Employees) object).printUserBaseInfo();
            } else if(object instanceof Manager){
                printChar(tmpLayer);
                Manager manager = (Manager) object;
                manager.printUserBaseInfo();
                manager.printEmployeesInfo(tmpLayer);
            }
        }
    }

    // 打印若干字符
    protected void printChar(int layer){
        for(int j=0;j<layer*2;j++){
            System.out.print("-");
        }
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }

    public ArrayList getArrayList() {
        return arrayList;
    }

    public void setArrayList(ArrayList arrayList) {
        this.arrayList = arrayList;
    }
}

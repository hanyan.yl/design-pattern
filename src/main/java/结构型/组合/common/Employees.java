package 结构型.组合.common;

/**
 * 普通员工
 *
 * Created by 73546 on 2017/4/25.
 */
public class Employees {

    private String no;
    private String name;
    private String position;
    private float salary;

    // 构造方法

    public Employees(String no, String name, String position, float salary) {
        this.no = no;
        this.name = name;
        this.position = position;
        this.salary = salary;
    }

    // 打印基本信息
    public void printUserBaseInfo(){
        System.out.println("|"+this.no+" "+this.name+" "+ this.position+" "+ this.salary);
    }

    public String getNo() {
        return no;
    }

    public void setNo(String no) {
        this.no = no;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public float getSalary() {
        return salary;
    }

    public void setSalary(float salary) {
        this.salary = salary;
    }
}

package 结构型.桥接.common;

/**
 * Created by 73546 on 2017/4/25.
 */
public abstract class AbstractSwitch {

    // 打开开关
    public abstract void turnOn();
    // 照明
    public abstract void light();
    // 关闭开关
    public abstract void turnOff();

    // 开灯照明
    public final void makeLight(){
        this.turnOn();
        this.light();
        this.turnOff();
    }
}

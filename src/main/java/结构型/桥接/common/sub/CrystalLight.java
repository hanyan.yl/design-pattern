package 结构型.桥接.common.sub;

import 结构型.桥接.common.AbstractSwitch;

/**
 *
 * 水晶灯
 * Created by 73546 on 2017/4/25.
 */
public class CrystalLight extends AbstractSwitch{


    @Override
    public void turnOn() {
        System.out.println("水晶灯打开了。。。");
    }

    @Override
    public void light() {
        System.out.println("水晶灯照明");
    }

    @Override
    public void turnOff() {
        System.out.println("水晶灯关闭了。。。");
    }

    public final void makeRemoteLight(int operColor){

        this.turnOn();
        this.light();
        String color = "";
        switch (operColor){
            case 1:
                color = "暖色";
                break;
            case 2:
                color = "蓝色";
                break;
            case 3:
                color = "红色";
                break;
            default:
                color = "白色";
                break;
        }
        System.out.println(" ...现在是"+color+"!");
        this.turnOff();
    }
}

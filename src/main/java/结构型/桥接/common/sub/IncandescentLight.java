package 结构型.桥接.common.sub;

import 结构型.桥接.common.AbstractSwitch;

/**
 *
 * 白炽灯
 * Created by 73546 on 2017/4/25.
 */
public class IncandescentLight extends AbstractSwitch{


    @Override
    public void turnOn() {
        System.out.println("白炽灯打开了。。。");
    }

    @Override
    public void light() {
        System.out.println("白炽灯照明");
    }

    @Override
    public void turnOff() {
        System.out.println("白炽灯关闭了。。。");
    }
}

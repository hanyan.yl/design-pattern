package 结构型.桥接;

import 结构型.桥接.common.AbstractSwitch;
import 结构型.桥接.common.sub.CrystalLight;
import 结构型.桥接.common.sub.IncandescentLight;

/**
 * Created by 73546 on 2017/4/25.
 */
public class MainApp {

    public static void main(String[] args) {
        // 白炽灯实例
        AbstractSwitch light = new IncandescentLight();
        // 水晶灯实例
        CrystalLight light2 = new CrystalLight();
        System.out.println("-- 一般开关 --");
        light.makeLight();
        System.out.println("\n -- 遥控开关 --");
        light2.makeRemoteLight(1);
    }
}

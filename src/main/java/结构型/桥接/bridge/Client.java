package 结构型.桥接.bridge;

import 结构型.桥接.bridge.lights.ILight;
import 结构型.桥接.bridge.lights.impl.CrystalLight;
import 结构型.桥接.bridge.lights.impl.IncandescentLight;
import 结构型.桥接.bridge.switchs.BaseSwitch;
import 结构型.桥接.bridge.switchs.sub.RemoteControlSwitch;

/**
 * Created by 73546 on 2017/4/25.
 */
public class Client {

    public static void main(String[] args) {
        ILight incandescentLight = new IncandescentLight();
        ILight crystalLight = new CrystalLight();

        // 一般开关
        System.out.println("-- 一般开关 --");
        BaseSwitch switch1 = new BaseSwitch(incandescentLight);
        switch1.makeLight();

        System.out.println("\n-- 遥控开关 --");
        RemoteControlSwitch remoteControlSwitch = new RemoteControlSwitch(crystalLight);
        remoteControlSwitch.makeRemoteLight(1);
    }
}

package 结构型.桥接.bridge.switchs;

import 结构型.桥接.bridge.lights.ILight;

/**
 * Created by 73546 on 2017/4/25.
 */
public class BaseSwitch {

    protected ILight light;

    public BaseSwitch(ILight light) {
        this.light = light;
    }

    public final void makeLight(){
        this.light.electricConnected();
        this.light.light();
        this.light.electricClosed();
    }
}

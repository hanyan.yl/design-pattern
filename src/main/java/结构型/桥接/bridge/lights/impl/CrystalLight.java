package 结构型.桥接.bridge.lights.impl;

import 结构型.桥接.bridge.lights.ILight;

/**
 * Created by 73546 on 2017/4/25.
 */
public class CrystalLight implements ILight {
    public void electricConnected() {
        System.out.println("水晶灯被打开了。。。");
    }

    public void light() {
        System.out.println("水晶灯照明。。。");
    }

    public void electricClosed() {
        System.out.println("水晶灯被关闭了。。。");
    }
}

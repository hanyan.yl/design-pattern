package 结构型.桥接.bridge.lights;

/**
 * Created by 73546 on 2017/4/25.
 */
public interface ILight {

    // 接通电流
    public void electricConnected();

    // 照明
    public void light();

    // 断开电流
    public void electricClosed();
}

package 结构型.适配器.adapter;

import 结构型.适配器.power.AbsBasePower;
import 结构型.适配器.power.v12.IPower12;

/**
 * Created by 73546 on 2017/4/25.
 */
public class AdapterPower12Ext extends AbsBasePower implements IPower12 {

    public AdapterPower12Ext(AbsBasePower absBasePower) {
        super(absBasePower.getPower());
    }

    public void output12v() {
        // 获得外部电源值
        float powerFloat = this.getPower();
        // 电源转换
        if(powerFloat == 380){
            powerFloat = powerFloat/31.67f;
        }else if(powerFloat == 220){
            powerFloat = powerFloat/18.33f;
        }else if(powerFloat == 110){
            powerFloat = powerFloat/9.17f;
        }else{
            System.out.println("-- 不能适配电源！ --");
            return;
        }
        // 处理转换结果
        powerFloat = (int) (powerFloat * 10)/10.0f;
        System.out.println("--这是["+ powerFloat + this.getUnit() +"]电源！...");
    }
}

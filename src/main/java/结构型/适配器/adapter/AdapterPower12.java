package 结构型.适配器.adapter;

import 结构型.适配器.power.AbsBasePower;
import 结构型.适配器.power.v12.IPower12;

/**
 * Created by 73546 on 2017/4/25.
 */
public class AdapterPower12 implements IPower12 {

    // 待转换的对象
    private final AbsBasePower absBasePower;

    public AdapterPower12(AbsBasePower absBasePower) {
        this.absBasePower = absBasePower;
    }

    // 实现目标对象方法
    public void output12v() {
        // 获得外部电源值
        float powerFloat = this.absBasePower.getPower();
        // 电源转换
        if(powerFloat == 380){
            powerFloat = powerFloat/31.67f;
        }else if(powerFloat == 220){
            powerFloat = powerFloat/18.33f;
        }else if(powerFloat == 110){
            powerFloat = powerFloat/9.17f;
        }else{
            System.out.println("-- 不能适配电源！ --");
            return;
        }
        // 处理转换结果
        powerFloat = (int) (powerFloat * 10)/10.0f;
        System.out.println("--这是["+ powerFloat + this.absBasePower.getUnit() +"]电源！...");
    }
}

package 结构型.适配器.power.v220;

import 结构型.适配器.power.AbsBasePower;

/**
 * Created by 73546 on 2017/4/25.
 */
public class Power220 extends AbsBasePower implements IPower220 {

    public Power220() {
        super(220);
    }

    public void output220v() {
        System.out.println("--这是["+ this.getPower() + this.getUnit() +"]电源！...");
    }
}

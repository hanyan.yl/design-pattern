package 结构型.适配器.power.v12;

import 结构型.适配器.power.AbsBasePower;

/**
 * Created by 73546 on 2017/4/25.
 */
public class Power12 extends AbsBasePower implements IPower12 {

    public Power12(){
        super(12);
    }

    public void output12v() {
        System.out.println("--这是["+ this.getPower() + this.getUnit() +"]电源！...");
    }
}

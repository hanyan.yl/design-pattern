package 结构型.适配器.power;

/**
 * Created by 73546 on 2017/4/25.
 */
public abstract class AbsBasePower {

    // 电压值
    private float power;
    // 单位
    private String unit = "V";

    public AbsBasePower(float power) {
        this.power = power;
    }

    public float getPower() {
        return power;
    }

    public void setPower(float power) {
        this.power = power;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
}

package 结构型.适配器;

import 结构型.适配器.adapter.AdapterPower12;
import 结构型.适配器.power.v12.IPower12;
import 结构型.适配器.power.v12.Power12;
import 结构型.适配器.power.v220.Power220;

/**
 * Created by 73546 on 2017/4/25.
 */
public class Client {

    public static void main(String[] args) {
        Power220 power220 = new Power220();
        power220.output220v();

        IPower12 power12 = new Power12();
        power12.output12v();

        System.out.println("\n---- 电源适配转换中 --");
        IPower12 adapterPower12 = new AdapterPower12(power220);
        adapterPower12.output12v();
        System.out.println("---- 电源视频器转换结束！ --");
    }
}
